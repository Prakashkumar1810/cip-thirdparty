import React from 'react';

import './login.css';

function Login(){
    return(
        <div className="login">
            <div className="title">Login to Facenote.</div>
            <div className="butt" onClick={()=>{
                /*function encodeQueryData(data) {
                    const ret = [];
                    for (let d in data)
                      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
                    return ret.join('&');
                }*/
                const data = {
                    "clientid" : "5f87e49083e83725d8da8608",
                    "scopes" : "name,email,gender,phone,bloodgroup,country",
                    "redirecturi" : "http://localhost:3082/dashboard"
                }
                const searchParams = new URLSearchParams(data);
                const querystring = searchParams.toString();//encodeQueryData(dat);
                window.location.href="http://localhost:3080/login?"+querystring;
                }}>Login</div>
        </div>
    );
}

export default Login;