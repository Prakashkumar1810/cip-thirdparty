import React,{useEffect, useState} from 'react';
import {useLocation} from 'react-router-dom';

import './dashboard.css';

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function Dashboard(){
    
    const query = useQuery();
    const [userdata, setuserdata] = useState({});
    const [it,setit] = useState([]);
    let items = [];
    
    useEffect(()=>{
        getdata(query);
    },[]);
    
    async function getdata(){
        const data = {
            token: query.get("token")
        };
        let response = await fetch("http://localhost:3081/auth/getdata",{
            method: "POST",
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });
        let result = await response.json();
        console.log(result);
        for(let key in result){
            items.push(
                <tr key={key}>
                    <td>{key}</td>
                    <td>{result[key]}</td>
                </tr>
            );
        }
        setuserdata(result);
        console.log(items);
        setit(items);
    }
    
    return(
        <div className="dashboard">
            <h1>Dashboard</h1>
            <h3>Token obtained is {query.get("token")}</h3>
            <h2>Data obtained using the token is</h2>
            {/*<h3>{JSON.stringify(userdata)}</h3>*/}
            <table id="data">
                <tbody>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    {it}
                </tbody>
            </table>
        </div>
    );
}

export default Dashboard;